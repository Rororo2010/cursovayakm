<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();


Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/home',\App\Http\Controllers\Main\IndexController::class)->name('main.index')->middleware('auth');

Route::prefix('tasks')->middleware('auth')->group(function(){
    Route::get('/', \App\Http\Controllers\Task\IndexController::class)->name('task.index');
    Route::get('/create', \App\Http\Controllers\Task\CreateController::class)->name('task.create');
    Route::post('/', \App\Http\Controllers\Task\StoreController::class)->name('task.store');
    Route::get('/{task}/edit', \App\Http\Controllers\Task\EditController::class)->name('task.edit');
    Route::get('/{task}', \App\Http\Controllers\Task\ShowController::class)->name('task.show');
    Route::patch('/{task}', \App\Http\Controllers\Task\UpdateController::class)->name('task.update');
    Route::delete('/{task}', \App\Http\Controllers\Task\DeleteController::class)->name('task.delete');
});

Route::prefix('additional_services')->middleware('auth')->group(function(){
    Route::get('/', \App\Http\Controllers\AdditionalService\IndexController::class)->name('additional_service.index');
    Route::get('/create', \App\Http\Controllers\AdditionalService\CreateController::class)->name('additional_service.create');
    Route::post('/', \App\Http\Controllers\AdditionalService\StoreController::class)->name('additional_service.store');
    Route::get('/{additional_service}/edit', \App\Http\Controllers\AdditionalService\EditController::class)->name('additional_service.edit');
    Route::get('/{additional_service}', \App\Http\Controllers\AdditionalService\ShowController::class)->name('additional_service.show');
    Route::patch('/{additional_service}', \App\Http\Controllers\AdditionalService\UpdateController::class)->name('additional_service.update');
    Route::delete('/{additional_service}', \App\Http\Controllers\AdditionalService\DeleteController::class)->name('additional_service.delete');
});

Route::prefix('managers')->middleware('auth')->group(function(){
    Route::get('/', \App\Http\Controllers\Manager\IndexController::class)->name('manager.index');
    Route::get('/create', \App\Http\Controllers\Manager\CreateController::class)->name('manager.create');
    Route::post('//', \App\Http\Controllers\Manager\StoreController::class)->name('manager.store');
    Route::get('/{manager}/edit', \App\Http\Controllers\Manager\EditController::class)->name('manager.edit');
    Route::get('/{manager}', \App\Http\Controllers\Manager\ShowController::class)->name('manager.show');
    Route::patch('/{manager}', \App\Http\Controllers\Manager\UpdateController::class)->name('manager.update');
    Route::delete('/{manager}', \App\Http\Controllers\Manager\DeleteController::class)->name('manager.delete');
});

Route::prefix('cathalogs')->middleware('auth')->group(function(){
    Route::get('/', \App\Http\Controllers\Cathalog\IndexController::class)->name('cathalog.index');
    Route::get('/create', \App\Http\Controllers\Cathalog\CreateController::class)->name('cathalog.create');
    Route::post('/', \App\Http\Controllers\Cathalog\StoreController::class)->name('cathalog.store');
    Route::get('/{cathalog}/edit', \App\Http\Controllers\Cathalog\EditController::class)->name('cathalog.edit');
    Route::get('/{cathalog}', \App\Http\Controllers\Cathalog\ShowController::class)->name('cathalog.show');
    Route::patch('/{cathalog}', \App\Http\Controllers\Cathalog\UpdateController::class)->name('cathalog.update');
    Route::delete('/{cathalog}', \App\Http\Controllers\Cathalog\DeleteController::class)->name('cathalog.delete');
});

Route::prefix('clients')->middleware('auth')->group(function(){
    Route::get('/', \App\Http\Controllers\Client\IndexController::class)->name('client.index');
    Route::get('/create', \App\Http\Controllers\Client\CreateController::class)->name('client.create');
    Route::post('/', \App\Http\Controllers\Client\StoreController::class)->name('client.store');
    Route::get('/{client}/edit', \App\Http\Controllers\Client\EditController::class)->name('client.edit');
    Route::get('/{client}', \App\Http\Controllers\Client\ShowController::class)->name('client.show');
    Route::patch('/{client}', \App\Http\Controllers\Client\UpdateController::class)->name('client.update');
    Route::delete('/{client}', \App\Http\Controllers\Client\DeleteController::class)->name('client.delete');
});

Route::prefix('factories')->middleware('auth')->group(function(){
    Route::get('/', \App\Http\Controllers\Factory\IndexController::class)->name('factory.index');
    Route::get('/create', \App\Http\Controllers\Factory\CreateController::class)->name('factory.create');
    Route::post('/', \App\Http\Controllers\Factory\StoreController::class)->name('factory.store');
    Route::get('/{factory}/edit', \App\Http\Controllers\Factory\EditController::class)->name('factory.edit');
    Route::get('/{factory}', \App\Http\Controllers\Factory\ShowController::class)->name('factory.show');
    Route::patch('/{factory}', \App\Http\Controllers\Factory\UpdateController::class)->name('factory.update');
    Route::delete('/{factory}', \App\Http\Controllers\Factory\DeleteController::class)->name('factory.delete');
});

Route::prefix('histories')->middleware('auth')->group(function(){
    Route::get('/', \App\Http\Controllers\History\IndexController::class)->name('history.index');
    Route::get('/create', \App\Http\Controllers\History\CreateController::class)->name('history.create');
    Route::post('/', \App\Http\Controllers\History\StoreController::class)->name('history.store');
    Route::get('/{history}/edit', \App\Http\Controllers\History\EditController::class)->name('history.edit');
    Route::get('/{history}', \App\Http\Controllers\History\ShowController::class)->name('history.show');
    Route::patch('/{history}', \App\Http\Controllers\History\UpdateController::class)->name('history.update');
    Route::delete('/{history}', \App\Http\Controllers\History\DeleteController::class)->name('history.delete');
});

