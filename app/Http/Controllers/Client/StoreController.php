<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\Client;
use App\Http\Requests\Client\StoreRequest;

class StoreController extends Controller
{
    public function __invoke(StoreRequest $request){
        $data = $request->validated();
        Client::firstOrCreate([
            'client_email' => $data['client_email']
        ],$data);

        return redirect()->route('client.index');
    }
}
