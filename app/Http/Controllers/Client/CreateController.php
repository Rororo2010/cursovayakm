<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;

class CreateController extends Controller
{
    public function __invoke(){
        return view('client.create');
    }
}
