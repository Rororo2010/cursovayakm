<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\Client;

class IndexController extends Controller
{
    public function __invoke(){
        $clients = Client::all();
        return view('client.index', compact('clients'));
    }
}
