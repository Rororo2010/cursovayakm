<?php

namespace App\Http\Controllers\AdditionalService;

use App\Http\Controllers\Controller;
use App\Models\AdditionalService;
class ShowController extends Controller
{
    public function __invoke(AdditionalService $additional_service){
        return view('additional_service.show', compact('additional_service'));
    }
}
