<?php

namespace App\Http\Controllers\AdditionalService;

use App\Http\Controllers\Controller;
use App\Models\AdditionalService;

class IndexController extends Controller
{
    public function __invoke(){
        $additional_services = AdditionalService::all();
        return view('additional_service.index', compact('additional_services'));
    }
}
