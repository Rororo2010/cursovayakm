<?php

namespace App\Http\Controllers\AdditionalService;

use App\Http\Controllers\Controller;
use App\Models\AdditionalService;

class EditController extends Controller
{
    public function __invoke(AdditionalService $additional_service){
        return view('additional_service.edit', compact('additional_service'));
    }
}
