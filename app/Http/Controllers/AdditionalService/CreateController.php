<?php

namespace App\Http\Controllers\AdditionalService;

use App\Http\Controllers\Controller;

class CreateController extends Controller
{
    public function __invoke(){
        return view('additional_service.create');
    }
}
