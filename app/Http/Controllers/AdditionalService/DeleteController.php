<?php

namespace App\Http\Controllers\AdditionalService;

use App\Http\Controllers\Controller;
use App\Models\AdditionalService;

class DeleteController extends Controller
{
    public function __invoke(AdditionalService $additional_service){
        $additional_service->delete();

        return redirect()->route('additional_service.index');
    }
}
