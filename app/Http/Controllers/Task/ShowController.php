<?php

namespace App\Http\Controllers\Task;

use App\Http\Controllers\Controller;
use App\Models\Task;
use App\Models\Cathalog;

class ShowController extends Controller
{
    public function __invoke(Task $task, Cathalog $cathalog){
        //$clients = Client::all();
        //$cathalog = $cathalog->where('task_id', $task->id)->get();
        return view('task.show', compact('task'));
    }
}
