<?php

namespace App\Http\Controllers\Task;

use App\Http\Controllers\Controller;
use App\Models\Task;
use App\Models\Manager;
use App\Models\History;
use App\Http\Requests\Task\StoreRequest;

class StoreController extends Controller
{
    public function __invoke(StoreRequest $request){
        $managers = Manager::all();
        $histories = History::all();
        $data = $request->validated();
        Task::firstOrCreate($data);

        return redirect()->route('task.index')->with([
            'histories'=> $histories,

        ]);
    }
}
