<?php

namespace App\Http\Controllers\Task;

use App\Http\Controllers\Controller;
use App\Models\Task;
use App\Models\History;

class EditController extends Controller
{
    public function __invoke(Task $task){
        $histories = History::all();
        return view('task.edit', compact('task'))->with([
            'histories'=> $histories,

        ]);
    }
}
