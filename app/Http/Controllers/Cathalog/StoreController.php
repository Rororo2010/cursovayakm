<?php

namespace App\Http\Controllers\Cathalog;

use App\Http\Controllers\Controller;
use App\Models\Cathalog;
use App\Http\Requests\Cathalog\StoreRequest;

class StoreController extends Controller
{
    public function __invoke(StoreRequest $request){
        $data = $request->validated();
        Cathalog::firstOrCreate($data);

        return redirect()->route('cathalog.index');
    }
}
