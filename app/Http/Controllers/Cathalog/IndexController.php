<?php

namespace App\Http\Controllers\Cathalog;

use App\Http\Controllers\Controller;
use App\Models\Cathalog;


class IndexController extends Controller
{
    public function __invoke(){
        $cathalogs = Cathalog::with('Factory')->get();
        return view('cathalog.index',
            compact('cathalogs'));
    }
}
