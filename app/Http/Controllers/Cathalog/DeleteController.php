<?php

namespace App\Http\Controllers\Cathalog;

use App\Http\Controllers\Controller;
use App\Models\Cathalog;


class DeleteController extends Controller
{
    public function __invoke(Cathalog $cathalog){
        $cathalog->delete();

        return redirect()->route('cathalog.index');
    }
}
