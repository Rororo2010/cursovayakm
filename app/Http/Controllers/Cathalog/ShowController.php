<?php

namespace App\Http\Controllers\Cathalog;

use App\Http\Controllers\Controller;
use App\Models\Cathalog;


class ShowController extends Controller
{
    public function __invoke(Cathalog $cathalog){
        return view('cathalog.show', compact('cathalog'));
    }
}
