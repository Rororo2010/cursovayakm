<?php

namespace App\Http\Controllers\Cathalog;

use App\Http\Controllers\Controller;
use App\Models\Cathalog;


class EditController extends Controller
{
    public function __invoke(Cathalog $cathalog){
        return view('cathalog.edit', compact('cathalog'));
    }
}
