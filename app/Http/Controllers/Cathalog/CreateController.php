<?php

namespace App\Http\Controllers\Cathalog;

use App\Http\Controllers\Controller;
use App\Models\Factory;


class CreateController extends Controller
{
    public function __invoke(){
        $factories = Factory::all();
        return view('cathalog.create')->with([
            'factories'=> $factories,
        ]);
    }
}
