<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use App\Models\Manager;

class DeleteController extends Controller
{
    public function __invoke(Manager $manager){
        $manager->delete();

        return redirect()->route('manager.index');
    }
}
