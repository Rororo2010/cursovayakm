<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use App\Models\Manager;

class EditController extends Controller
{
    public function __invoke(Manager $manager){
        return view('manager.edit', compact('manager'));
    }
}
