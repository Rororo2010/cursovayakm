<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;

class CreateController extends Controller
{
    public function __invoke(){
        return view('manager.create');
    }
}
