<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use App\Models\Manager;

class IndexController extends Controller
{
    public function __invoke(){
        $managers = Manager::all();
        return view('manager.index', compact('managers'));
    }
}
