<?php

namespace App\Http\Controllers\Factory;

use App\Http\Controllers\Controller;
use App\Models\Factory;

class EditController extends Controller
{
    public function __invoke(Factory $factory){
        return view('factory.edit', compact('factory'));
    }
}
