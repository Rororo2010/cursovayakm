<?php

namespace App\Http\Controllers\Factory;

use App\Http\Controllers\Controller;
use App\Models\Factory;

class ShowController extends Controller
{
    public function __invoke(Factory $factory){
        return view('factory.show', compact('factory'));
    }
}
