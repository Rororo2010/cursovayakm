<?php

namespace App\Http\Controllers\Factory;

use App\Http\Controllers\Controller;
use App\Models\Factory;

class DeleteController extends Controller
{
    public function __invoke(Factory $factory){
        $factory->delete();

        return redirect()->route('factory.index');
    }
}
