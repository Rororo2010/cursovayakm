<?php

namespace App\Http\Controllers\Factory;

use App\Http\Controllers\Controller;
use App\Models\Factory;

class IndexController extends Controller
{
    public function __invoke(){
        $factories = Factory::all();
        return view('factory.index', compact('factories'));
    }
}
