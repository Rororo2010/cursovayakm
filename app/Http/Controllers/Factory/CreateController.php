<?php

namespace App\Http\Controllers\Factory;

use App\Http\Controllers\Controller;

class CreateController extends Controller
{
    public function __invoke(){
        return view('factory.create');
    }
}
