<?php

namespace App\Http\Controllers\History;

use App\Http\Controllers\Controller;
use App\Models\Factory;

class CreateController extends Controller
{
    public function __invoke(){
        $factories = Factory::all();
        return view('history.create')->with([
            'factories'=> $factories,
        ]);
    }
}
