<?php

namespace App\Http\Controllers\History;

use App\Http\Controllers\Controller;
use App\Models\History;

class DeleteController extends Controller
{
    public function __invoke(History $history){
        $history->delete();

        return redirect()->route('history.index');
    }
}
