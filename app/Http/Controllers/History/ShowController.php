<?php

namespace App\Http\Controllers\History;

use App\Http\Controllers\Controller;
use App\Models\History;

class ShowController extends Controller
{
    public function __invoke(History $history){
        return view('history.show', compact('history'));
    }
}
