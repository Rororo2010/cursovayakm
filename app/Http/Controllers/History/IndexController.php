<?php

namespace App\Http\Controllers\History;

use App\Http\Controllers\Controller;
use App\Models\History;

class IndexController extends Controller
{
    public function __invoke(){
        $histories = History::all();
        return view('history.index', compact('histories'));
    }
}
