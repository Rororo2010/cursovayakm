<?php

namespace App\Http\Controllers\History;

use App\Http\Controllers\Controller;
use App\Models\History;

class EditController extends Controller
{
        public function __invoke(History $history){
            return view('history.edit', compact('history'));
        }
}

