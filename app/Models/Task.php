<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $table = 'tasks';
    protected $guarded = false;

    public function manager(){
        return $this->belongsTo(Manager::class, 'manager_id', 'id');
    }

    public function client(){
        return $this->belongsTo(Client::class, 'client_id', 'id');
    }

    public function cathalog(){
        return $this->belongsTo(Cathalog::class, 'cathalog_id', 'id');
    }
}
